<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = array_keys(config('app.categories'));
        $cat_1 = config('app.categories.'.$keys[1]);
        foreach (config('app.categories.'.$keys[0]) as $id => $name) {
            DB::table('categories')->insert([
                'id' => $id,
                'name' => $name,
                'name_'.$keys[1] => $cat_1[$id],
                'order' => config('app.categories.order.'.$id),
            ]);
        }
    }
}
