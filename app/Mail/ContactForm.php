<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    private $data;
    private $lang;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($lang, array $data)
    {
        $this->lang = $lang;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $username = '';
        $send_copy = false;
        if (isset($this->data['send_copy'])) {
            $send_copy = true;
            $this->cc($this->data['email']);
            if (isset($this->data['name'])) {
                $username = ' '.$this->data['name'];
            }
        }

        return $this
                    ->from(config('app.mail.sender', 'drkdidel.be contact form'))
                    ->subject(
                        config('app.contact.subjects.'.$this->lang)[$this->data['subject']]
                    )
                    ->view('contacts.email')
                    ->with([
                        // message var already exists in view
                        'mail_content' => $this->data['message'],
                        'send_copy' => $send_copy,
                        'lang' => $this->lang,
                        'username' => $username,
                    ])
                ;
    }
}
