<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Post;

class NewComment extends Mailable
{
    use Queueable, SerializesModels;

    private $post;
    private $username;
    private $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Post $post, String $username, String $comment)
    {
        $this->post = $post;
        $this->username = $username;
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('app.mail.sender', 'drkdidel.be comment form'))
            ->subject('New comment on '.$this->post->title)
            ->text('emails.new_comment')
            ->with([
                'post'     => $this->post,
                'username' => $this->username,
                'comment'  => $this->comment,
            ]);
    }
}
