<?php

namespace App\Http;

/**
 * Custom Request.
 * @todo I can extend Illuminate\Http\Request but
 * how to replace Illuminate\Support\Facades\Request with my own facade ?
 * how does facades know which class to use ?
 * @see config/app.php
 **/
class Request
{
    public function segmentsFrom($segNum)
    {
        $segNum--;
        $segments = request()->segments();
        $segmentsRemaining = [];
        $segmentsUri = '';
        for ($segNum; $segNum < count($segments); $segNum++) {
            array_push($segmentsRemaining, $segments[$segNum]);
        }
        if ($segmentsRemaining) {
            $segmentsUri = '/'.implode('/', $segmentsRemaining);
        }
        return $segmentsUri;
    }

    public function language()
    {
        $language = 'en';
        if (in_array(request()->segment(1), array_keys(config('app.languages')))) {
            $language = request()->segment(1);
        }
        return $language;
    }
}
