<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function store($name, $name_fr, $order)
    {
        $category = Category::create([
            'name' => $name,
            'name_fr' => $name_fr,
            'order' => $order,
        ]);
        session()->flash('message', 'Your category '.$category->name.' has been created.');
        return back();
    }
}
