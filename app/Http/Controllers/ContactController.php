<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactForm;
use Validator;

class ContactController extends Controller
{
    public function index($lang)
    {
        //email
        //subject (choices: Job proposal, Suggestion, Other),
        //put them in config file
        //message,
        $subjects = config('app.contact.subjects.'.$lang);

        return view('contacts.form', compact('subjects'));
    }

    public function send(Request $request, $lang)
    {
        $validator = Validator::make($request->all(), [
            'subject'    => 'required|integer',
            'message'    => 'required',
            'email_main' => 'honeypot',
            'alias'      => 'required|honeytime:5',
            'send_copy'  => 'boolean|nullable',
            'name'       => 'string|nullable',
        ]);

        $validator->sometimes('email', 'required|email', function ($input) {
            return $input->send_copy;
        });

        if ($validator->fails()) {
            return redirect($lang.'/contact')
                ->withErrors($validator)
                ->withInput();
        }

        Mail::to(config('app.contact.recipient'))->send(new ContactForm($lang, $validator->getData()));

        session()->flash('message', __('master.flashContact'));

        return back();
    }
}
