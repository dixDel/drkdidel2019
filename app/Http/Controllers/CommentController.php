<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Post;
use App\Mail\NewComment;

class CommentController extends Controller
{
    public function store(Request $request, $lang, $category, Post $post)
    {
        $validatedData = $request->validate([
            'comment' => 'required|string',
            'address' => 'honeypot',
            'city'    => 'required|honeytime:3',
            'name'    => 'string|nullable',
            //url forces full url http...
            //'website' => 'string|nullable',
        ]);

        if($post->comments()->create($validatedData)) {
            Mail::to(config('app.contact.recipient'))
                ->send(new NewComment($post, $validatedData['name'] ?? 'Anonymous', $validatedData['comment']));

            session()->flash('message', __('posts.comments.flashSuccess'));
        }

        return back();
    }
}
