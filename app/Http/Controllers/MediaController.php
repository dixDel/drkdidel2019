<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{
    public function delete($year, $slug, $filename)
    {
        $mediapath = 'public/'.$year.'/'.$slug.'/'.$filename;
        $isSuccess = Storage::delete($mediapath);

        return response()->json([
            'mediapath' => $mediapath,
            'isSuccess' => $isSuccess,
        ]);
    }
}
