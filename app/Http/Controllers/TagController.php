<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;

class TagController extends Controller
{
    public function postsByTag($lang, Tag $tag)
    {
        $baseQuery = $tag->posts()
            ->filterLanguage($lang)
            ->filterPublic();

        $postsCount = $baseQuery->count();

        $posts = $baseQuery
            ->latest()
            ->simplePaginate(5);

        $category = collect(['name' => __('tags.search_title').$tag->name]);

        return view('posts.index', compact('posts', 'category', 'postsCount'));
    }
}
