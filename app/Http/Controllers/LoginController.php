<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\LoginController as BaseLoginController;
use Illuminate\Support\Facades\Auth;

class LoginController extends BaseLoginController
{
    public function index()
    {
        $this->saveIntendedUrl();

        return view('login.index');
    }

    public function authenticate(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        if (Auth::attempt($validatedData)) {
            return redirect()->intended('/');
        } else {
            return back();
        }
    }

    public function logout(Request $request)
    {
        $this->saveIntendedUrl();

        Auth::logout();

        return redirect()->intended('/');
    }

    private function saveIntendedUrl()
    {
        if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }
    }
}
