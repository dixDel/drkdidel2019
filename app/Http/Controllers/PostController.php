<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreBlogPost;
use App\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Tag;

class PostController extends Controller
{
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
        if (!file_exists('./storage')) {
            symlink("../storage/app/public", "./storage");
        }
    }

    public function index($lang, Category $category)
    {
        $baseQuery = Post::where('category_id', $category->id)
            ->filterLanguage($lang)
            ->filterPublic();

        $postsCount = $baseQuery->count();

        $posts = $baseQuery
            ->latest()
            ->simplePaginate(5);

        return view('posts.index', compact('posts', 'category', 'postsCount'));
    }

    public function archives($lang, $year, $month)
    {
        $baseQuery = Post::filterLanguage($lang)
            ->filterArchives($year, $month)
            ->filterPublic();

        $postsCount = $baseQuery->count();

        $posts = $baseQuery
            ->latest()
            ->simplePaginate(5);

        $date = Carbon::create($year, $month);
        $category = collect(['name' => 'Archives: '.$date->formatLocalized('%B %Y')]);

        return view('posts.index', compact('posts', 'category', 'postsCount'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function edit($lang, Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    public function editNoSlug($lang, $id)
    {
        $post = Post::find($id);
        return $this->edit($lang, $post);
    }

    public function translate($lang, $id)
    {
        $post = Post::find($id);
        return view('posts.translate', compact('post'));
    }

    public function delete($lang, Post $post)
    {
        $post->delete();

        session()->flash('message', 'Your post '.$post->title.' has been deleted.');

        return redirect('/'.$post->language.'/posts/'.strtolower($post->category->name));
    }

    public function deleteNoSlug($lang, $id)
    {
        $post = Post::find($id);
        return $this->delete($lang, $post);
    }

    public function update(StoreBlogPost $request, $lang, Post $post)
    {
        $data = $request->validated();
        $data['slug'] = $post->generateSlug($data['title']);
        $data['is_featured'] = $request->has('is_featured') ? "1" : "0";
        $data['is_public'] = $request->has('is_public') ? "1" : "0";

        if ($post->update($data)) {
            $this->saveTags($post, $data);
            // save media
            $path = 'none';
            if ($request->file('media')) {
                $count = count($request->file('media'));
                for ($i = 0; $i < $count; $i++) {
                    $file = $request->file('media')[$i];
                    $filename = $data['media_name'][$i].'.'.$file->extension();
                    $path = $file->storeAs($post->mediaPath, $filename);
                }
            }
        }

        session()->flash('message', 'Your post '.$request->title.' has been updated. File stored in '.$path);

        // currently useless because slug is mandatory
        $segmentSlug = '';
        if ($post->slug !== '') {
            $segmentSlug = '/'.$post->slug;
        }
        return redirect('/'.$post->language.'/posts/'.strtolower($post->category->name).$segmentSlug);
    }

    public function updateNoSlug(StoreBlogPost $request, $lang, $id)
    {
        $post = Post::find($id);
        return $this->update($request, $lang, $post);
    }

    public function show($lang, Category $category, Post $post)
    {
        if (!$post->is_public && !Auth::check()) {
            return abort(404);
        }
        // check if post exists in other lang
        if ($lang !== $post->language) {
            if ($post->isTranslated()) {
                $post = $post->translatedPost();
            } else {
                return abort(404);
            }
        }
        return view('posts.show', compact('post', 'category'));
    }

    public function showPermalink($id)
    {
        if (!($post = Post::find($id))) {
            return abort(404);
        }

        return redirect('/'.$post->language.'/posts/'.strtolower($post->category->name).'/'.$post->slug);
    }

    public function store(StoreBlogPost $request)
    {
        $data = $this->finalizePost($request);

        $post = $this->createPost($data);
        $this->saveTags($post, $data);

        session()->flash('message', 'Your post '.$post->title.' has now been published.');

        return redirect('/'.$post->language.'/posts/'.strtolower($post->category->name));
    }

    private function createPost($data)
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return $user->posts()->create($data);
    }

    private function finalizePost(StoreBlogPost $request)
    {
        $data = $request->validated();
        // language is used to generate slug translation
        $this->post->language = $data['language'];
        $data['slug'] = $this->post->generateSlug($data['title']);
        return $data;
    }

    public function storeTranslation(StoreBlogPost $request, $lang, $id)
    {
        $data = $this->finalizePost($request);
        $data['translated_id'] = $id;

        $post = $this->createPost($data);

        session()->flash('message', 'The translation of your post '.$post->title.' has now been published.');

        return redirect('/'.$post->language.'/posts/'.strtolower($post->category->name));
    }

    // If I add $keywords as parameter:
    // Type error: Too few arguments to function App\Http\Controllers\PostController::search(), 2 passed and exactly 3 expected
    public function search(Request $request, $lang)
    {
        $validatedData = $request->validate([
            'keywords' => 'required|string|min:3',
        ]);
        $keywords = explode(' ', $validatedData['keywords']);

        $baseQuery;
        foreach ($keywords as $keyword) {
            if (!isset($baseQuery)) {
                $baseQuery = Post::where(function ($query) use ($keyword) {
                    $query->where('title', 'like', '%'.$keyword.'%');
                    $query->orWhere('body', 'like', '%'.$keyword.'%');
                });
            } else {
                $baseQuery->where(function ($query) use ($keyword) {
                    $query->where('title', 'like', '%'.$keyword.'%');
                    $query->orWhere('body', 'like', '%'.$keyword.'%');
                });
            }
        }
        $baseQuery
            ->filterLanguage($lang)
            ->filterPublic();

        $postsCount = $baseQuery->count();

        $posts = $baseQuery
            ->latest()
            ->simplePaginate(5);

        $search = true;

        return view('posts.index', compact('posts', 'postsCount', 'search', 'keywords'));
    }

    private function saveTags(Post $post, array $data)
    {
        // http://php.net/manual/en/function.explode.php#99830
        $tags = array_filter(explode(',', $data['tags']));
        $tag_ids = [];
        foreach ($tags as $tag) {
            $tag_ids[] = Tag::firstOrCreate(['name' => trim($tag)])
                ->id;
        }
        $post->tags()->syncWithoutDetaching($tag_ids);
    }
}
