<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

/*
 * @source https://stackoverflow.com/a/28403907/6548537
 */
class HttpsProtocol {

    public function handle($request, Closure $next)
    {
        if (!$request->secure() && App::environment() === 'production') {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}
