<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // @see routes/web.php
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category_id' => 'required|integer',
            'title'       => 'max:255',
            'body'        => 'required',
            'language'    => 'required|string',//@todo accept only en,fr
            'is_featured' => 'boolean',
            'is_public'   => 'boolean',
            'created_at'  => 'date',
            'tags'        => [
                'nullable',
                'max:50',
                'regex:/^[A-ZÀÂÇÉÈÊËÎÏÔÛÙÜŸÑÆŒa-zàâçéèêëîïôûùüÿñæœ0-9-\', ]*$/',
            ],
        ];
        //@todo check each media name for each media (empty media must be discarded)
        if ($this->file('media') && count($this->file('media')) > 0) {
            /*
             * fix svg: https://laracasts.com/discuss/channels/requests/image-validation-fails-with-svg
             * not working with mime image/svg+xml (svg stored as html)
             * https://laracasts.com/discuss/channels/requests/uploaded-svg-files-stored-as-html
             */
            $rules['media.*']      = 'image|max:10000';
            $rules['media_name.*'] = 'alpha_dash|max:255';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'category_id.required' => 'Please select a category.',
            'media.*'              => 'Accepted media formats are jpg, jpeg, png, gif and svg (not svg+xml, max size 10kb).',
            'tags.regex'           => 'Tags can only contain letters, digits, -, \' and spaces.',
        ];
    }
}
