<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Parsedown;
use App\Tag;
use App\Comment;

class Post extends Model
{
    private $translatedPost;
    private $savedMatches;

    protected $fillable = [
        'title',
        'body',
        'is_featured',
        'is_public',
        'slug',
        'language',
        'category_id',
        'created_at',
        'translated_id',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function highlightTitle(array $keywords)
    {
        $title = $this->title;
        foreach ($keywords as $keyword) {
            $title = $this->highlightKeyword($keyword, $title);
        }
        return $title;
    }

    private function highlightKeyword($keyword, $text)
    {
        $keyword = str_ireplace(
            ['`','$','[','(','"','\'','<','>',']',')','{','}','/'],
            '',
            $keyword
        );
        if (!isset($keyword) || empty($keyword)) {
            return $text;
        }
        return preg_replace('/(?!<[a-z]+[^>]*)('.$keyword.')(?![^<]*>)/i', '<span class="search-highlighted">$1</span>', $text);
    }

    public function highlightBody(array $keywords)
    {
        $body = $this->bodyHtml();
        foreach ($keywords as $keyword) {
            $body = $this->highlightKeyword($keyword, $body);
        }
        return $body;
    }

    public function bodyHtml()
    {
        $parsedown = new Parsedown;
        return $parsedown->text($this->bodyFiltered());
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    // getYearAttribute breaks archives date query
    public function getCreatedYearAttribute()
    {
        $date = Carbon::parse($this->created_at);
        return $date->year;
    }

    public function getMediaPathAttribute()
    {
        return 'public/'.$this->createdYear.'/'.$this->slug;
    }

    public function getMediaAttribute()
    {
        // this method only looks inside of your application's storage folder, usually storage/app
        // https://stackoverflow.com/a/43092035
        $files = Storage::files($this->mediaPath);
        $files_url = [];
        foreach ($files as $file) {
            $files_url[] = Storage::url($file);
        }
        return $files_url;
    }

    /**
     * Check if the post exist in the other language.
     *
     * Either the post is a translation and translated_id is set;
     * either this is the original post and its id is referenced in a translation.
     *
     * Save translatedPost to avoid redoing the same query in translatedPost.
     *
     * @return bool
     */
    public function isTranslated()
    {
        $this->translatedPost = $this->where('translated_id', $this->id)->first();
        return $this->translated_id || $this->translatedPost;
    }

    /**
     * Return translated post.
     *
     * This should be called after checking isTranslated.
     *
     * @return Post or null
     */
    public function translatedPost()
    {
        if ($this->translatedPost) {
            return $this->translatedPost;
        } else {
            return $this->find($this->translated_id);
        }
    }

    /**
     * Truncate body to 400 chars and remove markdown links.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return self
     * @todo using local scope to avoid static, but this has nothing to do with
     * the query and that's probably very bad :)
     */
    public function scopeTruncate($query)
    {
        $this->body = preg_replace(
            [
                '/\(http[^)]*\)?/',
                '/\[([^\]]*)\]/',
            ],
            [
                '',
                '$1'
            ],
            substr($this->body, 0, 400)
        );
        return $this;
    }

    public function scopeTranslateAndTruncate($query, $id, $lang)
    {
        $post = $this->find($id) ?? new Post;
        if ($post->language !== $lang && $post->isTranslated()) {
            $post = $post->translatedPost();
        }
        return $post->scopeTruncate($query);
    }

    public function scopeArchives($query, $lang)
    {
        return Post::selectRaw('YEAR(created_at) year, MONTH(created_at) month, COUNT(*) nb_posts')
            ->where('language', $lang)
            ->where('is_public', true)
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) DESC')
            ->get()
            ->toArray();
    }

    public function scopeFilterArchives($query, $year, $month)
    {
        $query->whereMonth('created_at', $month);
        $query->whereYear('created_at', $year);
    }

    public function scopeFilterLanguage($query, $lang)
    {
        $query->where('language', $lang);
    }

    public function scopeFilterPublic($query)
    {
        if (!Auth::check()) {
            $query->where('is_public', 1);
        }
    }

    /**
     * Remove HTML tags and attributes which are not whitelisted.
     *
     * @return string The filtered body
     */
    private function bodyFiltered()
    {
        // 1) isolate backticks blocks
        // 2) exec code below
        // 3) replace backticks blocks
        $bodyFiltered = preg_replace(
            [
                '/\<(?!\/?(a|abbr|span|sub|sup))[^\>]+\>/',
                '/\<[^\s\>]+\s(?!class|id|title)[^\>]*\>/',
            ],
            [
                '',
                '',
            ],
            $this->replaceCodeBlocks()
        );
        return $this->restoreCodeBlocks($bodyFiltered);
    }

    /**
     * Replace markdown code blocks ``` with placeholders
     */
    private function replaceCodeBlocks()
    {
        $savedMatches;
        $nb = 1;
        $filteredBody = preg_replace_callback(
            '/`(``)?[^`]+`(``)?/',
            function($matches) use (&$nb, &$savedMatches) {
                $savedMatches[$nb] = $matches[0];
                return '```'.$nb++;
            },
            $this->body
        );
        $this->savedMatches = $savedMatches;
        return $filteredBody;
    }

    /**
     * Restore code blocks in their original place.
     */
    private function restoreCodeBlocks($bodyFiltered)
    {
        $savedMatches = $this->savedMatches;
        $nb = 1;
        return preg_replace_callback(
            '/```[1-9][0-9]*/',
            function($matches) use ($savedMatches, &$nb) {
                return $savedMatches[$nb++];
            },
            $bodyFiltered
        );
    }

    /**
     * Generate slug from given $title.
     *
     * - strtolower
     * - only keeps alphanum and dashes
     * - must start end finish with a word
     * - replace non ASCII chars with their ASCII counterparts
     * - limit to 100 chars (97 + room for -lg if slug is not unique)
     * - make slug unique
     */
    public function generateSlug($title)
    {
        return $this->makeSlugUnique(strtolower(preg_replace(
            [
                '/[^a-zA-Z0-9 -]/',
                '/[ -]+/',
                '/^-|-$/',
            ],
            [
                '',
                '-',
                '',
            ],
            $this->sanitizeSlug(substr($title, 0, 97))
        )));
    }

    /**
     * Add language to slug if it is already used in other language.
     *
     * This happens when a translation use the same title.
     * @param string $slug
     * @return string
     */
    private function makeSlugUnique($slug)
    {
        if ($this->where('slug', $slug)
            ->where('language', '!=', $this->language)
            ->count() > 0
        ) {
            $slug .= '-'.$this->language;
        }
        return $slug;
    }

    private function sanitizeSlug($slug)
    {
        $patterns = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
        $replacements = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        return str_replace($patterns, $replacements, $slug);
    }
}
