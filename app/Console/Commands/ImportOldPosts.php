<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Post;
use App\User;
use League\HTMLToMarkdown\HtmlConverter;

class ImportOldPosts extends Command
{
    private $post;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:oldPosts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import old drkdidel articles from exported php array';

    /**
     * Create a new command instance.
     *
     * @source https://stackoverflow.com/a/24887515
     */
    public function __construct(Post $post)
    {
        parent::__construct();
        $this->post = $post;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        include 'olddata/drkdideltest_table_articles.php';
        include 'olddata/drkdideltest_table_htmlTags.php';
        $tags = array_column($htmlTags, 'articleId');
        // single symbols need to be after to avoid breaking previous patterns
        $patterns = [
            '/Ã´/i',
            '/Ã‰/i',
            '/â€™/i',
            '/Ã©/i',
            '/Ã®/i',
            '/Â/i',
            '/Ã¨/i',
            '/Ãª/i',
            '/Ã§/i',
            '/Ã¢/i',
            '/à/i',
            '/Ã/i',
        ];
        $replacements = [
            'ô',
            'É',
            '\'',
            'é',
            'î',
            '',
            'è',
            'ê',
            'ç',
            'â',
            'ù',
            'à',
        ];
        foreach ($articles as $article) {
            if ($article['title'] !== "") {
                $article['title'] = preg_replace(
                    $patterns,
                    $replacements,
                    $article['title']
                );
                // @source http://php.net/manual/en/function.preg-replace.php
                $article['slug'] = $this->post->generateSlug($article['title']);
            } else {
                unset($article['title']);
                unset($article['slug']);
            }
            $article = $this->restoreTags($htmlTags, $tags, $article);
            $article['body'] = preg_replace(
                $patterns,
                $replacements,
                $this->htmlToMarkdown($article['text'])
            );
            $article['category_id'] = $article['categoryId'];
            $article['is_featured'] = $article['isFeatured'];
            $article['is_public'] = $article['isPublic'];
            // Laravel auto converts old id 1, 2 to en, fr !
            $article['language'] = $article['languageId'];
            $article['translated_id'] = $article['translationOfId'];
            $article['created_at'] = $article['createdAt'];
            $user = User::find($article['createdBy']);
            $user->posts()->create($article);
        }
    }

    private function restoreTags($htmlTags, $tags, $article)
    {
        while(($key = array_search($article['id'], $tags)) !== false) {
            $tag = $this->processTags($htmlTags[$key]['tag']);
            $position = $htmlTags[$key]['position'];
            if ((int)$position < 10) {
                $position = '0'.$position;
            }
            $placeholder = '<$'.$position.'$>';
            $article['text'] = str_replace($placeholder, $tag, $article['text']);
            unset($tags[$key]);
        }
        return $article;
    }

    private function processTags($tag)
    {
        return preg_replace(
            '/(<\/?)span( class="code")?>/',
            '$1code>',
            $tag
        );
    }

    private function htmlToMarkdown($html)
    {
        $converter = new HtmlConverter();
        return $converter->convert($html);
    }
}
