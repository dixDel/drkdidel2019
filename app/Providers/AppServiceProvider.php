<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Category;
use App\Http\Request as CustomRequest;
use Parsedown;
use App\Post;
use App;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $lang = $this->app['request']->segment(1);
        if(array_key_exists($lang, config('app.languages'))) {
            //@source https://stackoverflow.com/a/40149785
            setlocale(LC_TIME, config('app.languages.'.$lang));
            App::setLocale($lang);
        } else {
            $lang = 'en';
        }

        view()->composer([
            'master',
            'layouts.header',
            'posts.index',
            'posts.show',
        ], function($view) use ($lang) {
            $view->with(compact('lang'));
        });

        view()->composer([
            'layouts.header',
            'layouts.sidebar',
        ], function($view) {
            $cusReq = new CustomRequest;

            $view->with(compact('cusReq'));
        });

        view()->composer([
            'layouts.header',
        ], function($view) {
            if (auth()->check()) {
                $categories = Category::has('posts');
            } else {
                $categories = Category::whereHas('posts', function($query) {
                    $query->where('is_public', true);
                });
            }
            $categories = $categories->orderBy('order')->get();

            $view->with(compact('categories'));
        });

        view()->composer([
            'posts.form'
        ], function($view) {
            $categories = Category::orderBy('order')->get();

            $view->with(compact('categories'));
        });

        view()->composer([
            'layouts.sidebar',
        ], function($view) use ($lang){
            // "Who am I" post id in db
            $about = Post::translateAndTruncate(1000000021, $lang);

            $archives = Post::archives($lang);

            $parsedown = new Parsedown;

            $view->with(compact('lang', 'about', 'archives', 'parsedown'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
