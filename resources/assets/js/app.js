
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

//const app = new Vue({
    //el: '#app'
//});

var mediaContNb = 0;
$('#media_add').click(function() {
    var $parent = $(this).parent();
    var $mediaCont = $('#mediaContainer0', $parent).clone();
    mediaContNb++;
    $mediaCont.attr('id', 'mediaContainer'+mediaContNb);
    $('[id=media0]', $mediaCont)
        .attr('name', 'media['+mediaContNb+']')
        .attr('id', 'media'+mediaContNb)
        .val('');
    $('[id=mediaName0]', $mediaCont)
        .attr('name', 'media_name['+mediaContNb+']')
        .attr('id', 'mediaName'+mediaContNb)
        .val('');
    $('.media_containers', $parent).append($mediaCont);
});
$('.media-delete').click(function() {
    var $mediaContainer = $(this).parent();
    var getUrl = window.location;
    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

    var urlPublic = $(this).data('mediafile');
    var urlPublicPrefix = '/storage';
    var url = baseUrl+'/media'+urlPublic.substring(
        urlPublic.indexOf(urlPublicPrefix) + urlPublicPrefix.length
    )+'/delete';

    $.get(url, function(data) {
        if (data.isSuccess) {
            $mediaContainer.remove();
            message = " has been successfully deleted.";
        } else {
            message = " could not be deleted.";
        }
        alert("Media "+data.mediapath+message);
    });
});
$('#send_copy').click(function() {
    if ($(this).prop('checked')) {
        $('#email', '#contact_email').removeAttr('disabled');
        $('#name', '#contact_name').removeAttr('disabled');
        $('label', '#contact_email').removeClass('disabled');
        $('label', '#contact_name').removeClass('disabled');
    } else {
        $('#email', '#contact_email').attr('disabled', 'disabled');
        $('#name', '#contact_name').attr('disabled', 'disabled');
        $('label', '#contact_email').addClass('disabled');
        $('label', '#contact_name').addClass('disabled');
    }
});
