<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@lang('master.title')</title>

        <link rel="icon" href="../../../../favicon.ico">
        <!-- Custom styles for this template -->
        <link href="/css/app.css" rel="stylesheet">
        <link href="/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
    </head>

    <body>

        <div class="container">
            @if(session('message'))
                <p class="alert alert-success alert-dismissible fade show flashMessage" role="alert">
                {{ session('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </p>
            @endif

            @include('layouts.header')

            @yield('featured')
        </div>

        <main role="main" class="container">
            <div class="row">
                <div class="col-md-8 blog-main">
                    @if(isset($category))
                        <h2 class="page-title font-italic border-bottom">{{ $category['name_'.$lang] ?? $category['name'] }}</h2>
                    @elseif(isset($search))
                        <h2 class="page-title font-italic border-bottom">@lang('master.search')@if(isset($keywords)): {{ implode(' ', $keywords) }} @endif</h2>
                    @endif
                    @isset($postsCount)
                    <div class="clearfix">
                        <p class="float-right font-italic m-0">{{ $postsCount }}
                        @if($postsCount > 1)
                            @lang('master.nbPosts')
                        @else
                            @lang('master.nbPosts-single')
                        @endif
                        </p>
                    </div>
                    @endisset

            @yield('content')

                </div><!-- /.blog-main -->

                @include('layouts.sidebar')
            </div><!-- /.row -->
        </main><!-- /.container -->

        @include('layouts.footer')

        <script src="/js/app.js"></script>

    </body>
</html>
