<div class="clearfix">
    {{--<p class="blog-post-meta float-left">--}}
    <div class="blog-post-meta float-left dropdown pr-3">
        <div>
            <button class="btn btn-sm btn-light" type="button" data-toggle="dropdown" title="Permalink" aria-haspopup="true" aria-expanded="false"><span class="oi oi-link-intact" title="Permalink" aria-hidden="true"></span></button>
            {{ $post->created_at->toFormattedDateString() }} @lang('posts.by') <a href="{{ Route('home', ['lang' => $lang, 'category' => 'about']) }}">{{ $post->user->name }}</a>
            <div class="dropdown-menu bg-light">
                <form class="px-2">
                    <div class="form-group">
                        <label for="{{ 'permalink'.$post->id }}">Permalink</label>
                        <input type="text" class="form-control" value="{{ Route('permalink', ['id' => $post->id, 'post' => $post->slug]) }}" id="{{ 'permalink'.$post->id }}">
                    </div>
                </form>
            </div>
        </div>
        <div>
            @php $nb_comment = count($post->comments); @endphp
            <p class="mb-0"><a href="{{ Route('show', [
                'lang' => $lang,
                'category' => strtolower($post->category->name),
                'post' => $post->slug
            ]) }}#comments" title="@lang('posts.comments_show')">{{ $nb_comment }} @if($nb_comment <= 1) @lang('posts.comments_count_singular') @else @lang('posts.comments_count') @endif</a></p>
        </div>
    </div>
    <div class="text-right">
        @if(count($post->tags) > 0)
            <p>
            @foreach($post->tags as $tag)
                <a href="{{ Route('postsByTag', ['lang' => $lang, 'tag' => $tag->name]) }}" title="@lang('posts.tag_title') {{ $tag->name }}" class="badge badge-pill badge-primary">{{ $tag->name }}</a>
            @endforeach
            </p>
        @endif
        @auth
            <p class="mb-0">
            <a href="/drekMansion/{{ Request::segment(1).'/posts/'.$post->id }}/translate" class="{{ $post->isTranslated() ? 'disabled' : '' }}">Translate</a>
            {{--PHP ternary operator https://stackoverflow.com/questions/34571330/php-ternary-operator-vs-null-coalescing-operator#34571460--}}
            - <a href="/drekMansion/{{ Request::segment(1).'/posts/'.($post->slug ?: $post->id) }}/edit">Edit</a>
            - <a href="/drekMansion/{{ Request::segment(1).'/posts/'.($post->slug ?: $post->id) }}/delete">Delete</a>
            </p>
        @endauth
    </div>
</div>
