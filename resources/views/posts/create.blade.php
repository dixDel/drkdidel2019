@extends('master')

@section('content')
    @component('posts.form', [
        'title' => 'Create a post',
        'action' => '/'.Request::segment(1).'/posts',
        'submit_legend' => 'Publish',
    ])
        @slot('method_spoof')@endslot
    @endcomponent
@endsection
