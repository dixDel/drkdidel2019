@if(!isset($keywords))
    @php $keywords = []; @endphp
@endif

@extends('master')

@section('content')
    @if(!$posts->count())
        <p class="text-center">@lang('posts.category_empty')</p>
    @endif
    @foreach($posts as $post)
        <div class="blog-post">
            <h3 class="blog-post-title {{ $post->is_public ? 'public' : 'not_public' }}">
                <a href="/{{ Request::segment(1).'/posts/'.strtolower($post->category->name).'/'.$post->slug }}">{!! $post->highlightTitle($keywords) !!}</a>
            </h3>
            @include('posts.data')
            {!! $post->highlightBody($keywords) !!}
        </div><!-- /.blog-post -->
    @endforeach

    @if(isset($keywords))
        @php $posts = $posts->appends(['keywords' => implode(',', $keywords)]); @endphp
    @endif
    {{ $posts->links('vendor/pagination/simple-bootstrap-4', ['paginator' => 'posts']) }}
@endsection
