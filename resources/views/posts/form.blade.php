<h2 class="page-title font-italic border-bottom">{{ $title }}</h2>
<form action="{{ $action }}" method="POST" enctype="multipart/form-data">
    {{ $method_spoof }}
    @csrf
    <div class="form-group">
        @if(isset($post))
            <p>Post ID, used for permalink: <em>{{ $post->id }}</em></p>
        @endif
        <label for="category_id">Category</label>
        <select class="form-control" id="category_id" name="category_id" required>
            {{--Empty value is needed for validation--}}
            {{--@source https://stackoverflow.com/questions/46405564/laravel-ignores-select-input-validation-if-no-option-is-selected--}}
            <option value="">Choose a category</option>
            @foreach($categories as $category)
                <option value="{{ $category->id }}"
                        {{--PHP7 null coalescing operator--}}
                        @if($category->id === (int) old('category_id', $post->category_id ?? 0))
                            selected
                        @endif
                        >{{ $category->name }}</option>
                    @endforeach
        </select>
        <p><em>Add category by using this URL: <a href="/drekMansion/categories/addEDITME/{name}/{name_fr}/{order}">/drekMansion/categories/add/{name}/{name_fr}/{order}</a></em><br/>
            Max length 20, only letters, digits, spaces and -. Order must be 1 to 3 digits
        </p>
    </div>
    @isset($created_at)
        <div class="form-group">
            <label for="created_at">Created at</label>
            <input id="created_at" class="form-control" type="text" name="created_at" value="{{ old('created_at', $post->created_at ?? '') }}"/>
        </div>
    @endisset
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $post->title ?? '') }}">
    </div>
    <div class="form-group">
        <label for="">Media</label>
        <div>
            @if(!isset($created_at))
                <p class="alert alert-warning text-center">Currently media are only saved on edit !</p>
            @endif
        @isset($post)
            @php $imgNb = 1 @endphp
            @foreach($post->media as $url)
                <div class="w-25 float-left mr-1">
                    <a href="{{ $url }}" title="{{ $url }}">
                        @php //@todo use proper way to create thumbnails @endphp
                        <img id="savedMedia{{ $imgNb }}" src="{{ $url }}" alt="{{ $url }}" width="200px"/>
                    </a>
                    <button id="mediaDeleteBtn{{ $imgNb }}" class="btn btn-danger btn-block media-delete" type="button" title="Delete media {{ $url }}" data-mediafile="{{ $url }}">DEL</button>
                </div>
                @php $imgNb++ @endphp
            @endforeach
        @endisset
        </div>
        <div class="media_containers">
            <div class="media_container" id="mediaContainer0">
                <div class="custom-file mt-1">
                    <input name="media[0]" id="media0" class="form-control-file" type="file" />
                </div>
                <input type="text" class="form-control" id="mediaName0" name="media_name[0]" placeholder="Enter a name for this file, without the extension"/>
            </div>
        </div>
        <button id="media_add" class="btn btn-primary btn-lg btn-block mt-1" type="button">Add media</button>
    </div>
    <div class="form-group">
        <label for="body">Body</label>
        <textarea id="body" class="form-control" name="body" cols="30" rows="10" required>{{ old('body', $post->body ?? '') }}</textarea>
    </div>
    <div class="form-group">
        @foreach([['en', 'English'], ['fr', 'Français']] as $language)
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="language" id="language_{{ $language[0] }}" value="{{ $language[0] }}" @if(old('language', $post->language ?? 'en') === $language[0]) checked @endif>
                <label class="form-check-label" for="language_{{ $language[0] }}">
                    {{ $language[1] }}
                </label>
            </div>
        @endforeach
    </div>
    <div class="form-group">
        <label for="tags">Tags</label>
        <textarea id="tags" class="form-control" name="tags" cols="30" rows="2" placeholder="Enter tags separated by comma">{{ old('tags') }}</textarea>
    </div>
    <div class="form-group">
        <div class="form-check form-check-inline">
            <input type="checkbox" class="form-check-input" id="is_featured" name="is_featured" @if(old('is_featured', $post->is_featured ?? false)) checked @endif value="1">
            <label class="form-check-label" for="is_featured">Featured</label>
        </div>
        <div class="form-check form-check-inline">
            {{--value is only sent if checked
                so if is_public is true in the DB, it will always be checked.
            Since it is a "dangerous" checkbox, I'll force the user to always check it.--}}
            <input type="checkbox" class="form-check-input" id="is_public" name="is_public" value="1">
            <label class="form-check-label" for="is_public">Public</label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">{{ $submit_legend }}</button>

    @include('layouts.errors')
</form>
<p class="text-right">
    <a href="{{ URL::previous() }}">Back</a>
</p>
