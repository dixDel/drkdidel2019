@extends('master')

@section('content')
    @component('posts.form', [
        'post' => $post,
        'title' => 'Edit a post',
        'action' => '/'.Request::segment(1).'/'.Request::segment(2).'/posts/'.($post->slug ?: $post->id),
        'submit_legend' => 'Update',
        'created_at' => true,
    ])
        @slot('method_spoof') @method('PATCH') @endslot
    @endcomponent
@endsection
