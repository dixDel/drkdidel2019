@extends('master')

@section('content')
    <div class="mb-5 blog-post">
        <h3 class="blog-post-title {{ $post->is_public ? 'public' : 'not_public' }}">{{ $post->title }}</h3>
        @include('posts.data')
        {!! $post->bodyHtml() !!}
        <p class="text-right">
            <a href="{{ URL::previous() }}">@lang('posts.back')</a>
        </p>
    </div>
    <hr />
    <div class="mb-5">
        <h4 class="font-italic" id="comments">@lang('posts.comments.title')</h4>
        @forelse($post->comments as $comment)
            <div class="px-5 pt-3">
                <p class="text-secondary mb-1">{{ $comment->created_at->toFormattedDateString() }} @lang('posts.comments.by') <em>{{ $comment->name ?? __('posts.comments.anonymous') }}</em></p>
                <p>{{ $comment->comment }}</p>
            </div>
        @empty
            <p class="text-center font-italic">@lang('posts.comments.no_comment')</p>
        @endforelse
        <p class="alert alert-primary text-center mt-5">@lang('posts.comments.warning')</p>
        <form method="POST">
            @csrf
            <div class="form-group">
                <label for="name">@lang('posts.comments.nameLabel')</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="@lang('posts.comments.namePlaceholder')" value="{{ old('name') }}">
                <small id="nameHelp" class="form-text text-muted">@lang('posts.comments.nameHelp')</small>
            </div>
            {!! Honeypot::generate('address', 'city') !!}
            <div class="form-group">
                <label for="comment">@lang('posts.comments.commentLabel')</label>
                <textarea id="comment" class="form-control" name="comment" cols="30" rows="10" placeholder="@lang('posts.comments.commentPlaceholder')" required>{{ old('comment') }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">@lang('posts.comments.send')</button>
        </form>
        @include('layouts.errors')
    </div>
@endsection
