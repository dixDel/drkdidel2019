@extends('master')

@section('content')
    @component('posts.form', [
        'post' => $post,
        'title' => 'Translate a post',
        'action' => '/'.Request::segment(1).'/'.Request::segment(2).'/posts/'.$post->id.'/translate',
        'submit_legend' => 'Translate',
    ])
        @slot('method_spoof')@endslot
    @endcomponent
@endsection
