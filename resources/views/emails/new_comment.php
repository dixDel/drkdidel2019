<?php
echo 'Sent by '.$username.':'.PHP_EOL.PHP_EOL;
echo $comment.PHP_EOL.PHP_EOL;
echo Route('show', [
    'lang'     => $post->language,
    'category' => strtolower($post->category->name),
    'post'     => $post->slug,
]).'#comments';
