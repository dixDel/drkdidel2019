@if($send_copy)
    <p>@lang('contact.hello'){{ $username }}, @lang('contact.foreword').</p>
    <hr/>
@endif
{{ $mail_content }}
<hr/>
<p style="text-align:center">
{{-- @todo use APP_URL--}}
    <a href="https://www.drkdidel.be" title="https://www.drkdidel.be">https://www.drkdidel.be</a>
</p>
