@extends('master')

@section('content')
    <h2 class="pb-3 mb-4 font-italic border-bottom">@lang('contact.title')</h2>
    <form action="" method="POST">
        @csrf
        <div class="form-group">
            <label for="subject">@lang('contact.subject')</label>
            <select class="form-control" id="subject" name="subject" required>
                {{--Empty value is needed for validation--}}
                {{--@source https://stackoverflow.com/questions/46405564/laravel-ignores-select-input-validation-if-no-option-is-selected--}}
                <option value="">@lang('contact.choose')</option>
                @foreach($subjects as $key => $subject)
                    <option value="{{ $key }}"
                        {{--PHP7 null coalescing operator--}}
                        @if($key === (int) old('subject'))
                            selected
                        @endif
                    >
                            {{ $subject }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="message">@lang('contact.message')</label>
            <textarea id="message" class="form-control" name="message" cols="30" rows="15" placeholder="@lang('contact.message_placeholder')" required>{{ old('message') }}</textarea>
        </div>
        {!! Honeypot::generate('email_main', 'alias') !!}
        <div class="form-group">
            <div class="form-check form-check-inline">
                <input type="checkbox" class="form-check-input" id="send_copy" name="send_copy" value="1"/>
                <label class="form-check-label" for="send_copy">@lang('contact.send_copy')</label>
            </div>
        </div>
        <div class="form-group" id="contact_email">
            <label for="email" class="disabled">@lang('contact.email')</label>
            <input id="email" class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="@lang('contact.email_placeholder')" disabled />
            <small id="emailHelp" class="form-text text-muted">@lang('contact.emailHelp')</small>
        </div>
        <div class="form-group" id="contact_name">
            <label for="name" class="disabled">@lang('contact.name')</label>
            <input id="name" class="form-control" type="text" name="name" value="{{ old('name') }}" placeholder="@lang('contact.name_placeholder')" disabled />
        </div>
        <button type="submit" class="btn btn-primary">@lang('contact.submit')</button>

        @include('layouts.errors')
    </form>
    <p class="text-right">
        <a href="{{ URL::previous() }}">@lang('contact.back')</a>
    </p>
@endsection
