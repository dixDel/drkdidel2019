<footer class="blog-footer">
    <div class="d-none d-md-block">
        @include('layouts.contribute')
    </div>
    <p class="mt-5">@lang('footer.license')</p>
    <p class="mt-3">@lang('footer.template')</p>
    <p class="mt-5">@lang('footer.back_top')</p>
</footer>
