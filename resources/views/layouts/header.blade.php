@php
    // disable language switch if the post is not translated
    $disabled = '';
    if (Route::currentRouteName() === 'show' && (Request::segment(2) == 'posts') && isset($post) && !$post->isTranslated()) {
        $disabled = ' disabled';
    }
@endphp

<header class="blog-header pt-1 pb-2">
    @auth
    <div class="row">
        <div class="col">
            <a class="btn btn-sm btn-outline-secondary" href="/drekMansion/leave">Log out</a>
            <a class="btn btn-sm btn-outline-secondary" href="/drekMansion/create">New post</a>
        </div>
    </div>
    @endauth
    <div class="row pb-1">
        <div class="col">
            <div class="btn-group float-left menu-mobile">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="oi oi-menu" title="Menu" aria-hidden="true"></span>
                </button>

                <div class="dropdown-menu">
                    @foreach(__('header.menu-mobile') as $key => $item)
                        <a class="dropdown-item" href="#sidebar-{{ $key }}" title="{{ $item }}">{{ $item }}</a>
                    @endforeach
                </div>
            </div>
            <h1 class="text-center mb-0">
                <a class="blog-header-logo text-dark" href="/{{ $lang.'/posts/'.strtolower(config('app.categories.en.1')) }}">drkdidel.be</a>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <form class="form-inline" action="/{{ $lang }}/search" method="GET">
                <div class="input-group">
                    <input name="keywords" id="keywords" class="form-control" type="text" placeholder="@lang('header.search')..." value="@if(isset($keywords)){{ implode(' ', $keywords) }}@endif" required/>
                    <button class="btn btn-primary" type="submit" title="@lang('header.search')"><span class="oi oi-magnifying-glass" title="@lang('header.search')" aria-hidden="true"></span></button>
                </div>
                @if ($errors->has('keywords'))
                    <div class="alert alert-danger m-0">
                        <ul class="list-group">
                            @foreach ($errors->all() as $error)
                                <li class="list-nobullet">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </form>
        </div>
        <div class="col-4 align-self-center text-right">
            @php $langs = array_keys(config('app.languages')) @endphp
            {{--Display the current language,--}}
            {{--get the next one to generate its link--}}
            @foreach($langs as $key => $abbr)
                @if($abbr === $lang)
                    @php
                        $otherLang = next($langs) ?: reset($langs);
                        $queryString = '';
                        if(($query = app('request')->getQueryString())) {
                            $queryString = "?$query";
                        }
                        if(\Request::route()->getName() === 'show') {
                            $translatedUrl = route('show', [
                                'lang' => $otherLang,
                                'category' => strtolower($post->category->name),
                                'post' => $post->translatedPost()->slug ?? $post->slug,
                                ]);
                        } else {
                            $translatedUrl = Request::segment(0).'/'.$otherLang.$cusReq->segmentsFrom(2);
                        }
                        $translatedUrl .= $queryString;
                    @endphp
                    <a class="btn btn-sm btn-outline-secondary{{ $disabled }}" href="{{ $translatedUrl }}" title="@lang('header.switch')">{{ strtoupper($abbr) }}</a>
                    @php prev($langs) ?: end($langs) @endphp
                @endif
                {{--foreach operates on a copy of the array,
                so I update the original manually--}}
                @php next($langs) @endphp
            @endforeach
        </div>
    </div>
</header>

<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
        @foreach($categories as $category)
            <a class="p-2 text-muted" href="/{{ $lang.'/posts/'.strtolower($category['name']) }}">{{ $category['name_'.$lang] ?: $category['name'] }}</a>
        @endforeach
    </nav>
</div>
