<aside class="col-md-4 blog-sidebar">
    @isset($about->title)
    <div class="p-3 mb-3 bg-light rounded">
        <h4 class="font-italic" id="sidebar-about">{{ $about->title }}</h4>
        <p class="mb-0">
            {!! $parsedown->text($about->body.' [\[...\]](/'.$cusReq->language().'/posts/'.strtolower($about->category->name).'/'.$about->slug.')') !!}
        </p>
    </div>
    @endisset

    <div class="p-3">
        <h4 class="font-italic" id="sidebar-contact">@lang('sidebar.contact.title')</h4>
        @lang('sidebar.contact.text1') <a href="/{{ $cusReq->language() }}/contact" title="@lang('sidebar.contact.link')">@lang('sidebar.contact.text2')</a>.
    </div>

    <div class="p-3">
        <h4 class="font-italic" id="sidebar-archives">Archives</h4>
        <ol class="list-unstyled mb-0">
            @foreach($archives as $archive)
                @php
                    $m = Carbon\Carbon::create($archive['year'], $archive['month']);
                @endphp
                <li><a href="/{{ $lang.'/posts/'.$archive['year'].'/'.$archive['month'] }}">{{ $m->formatLocalized('%B %Y') }}</a> ({{ $archive['nb_posts'] }})</li>
            @endforeach
        </ol>
    </div>

    <div class="p-3">
        <h4 class="font-italic" id="sidebar-elsewhere">@lang('sidebar.elsewhere')</h4>
        <ol class="list-unstyled">
            <li><a href="https://gitlab.com/dixDel">GitLab</a></li>
            <li><a href="https://github.com/dixDel">GitHub</a></li>
        </ol>
    </div>

    <div class="p-3">
        <h4 class="font-italic" id="sidebar-contribute">@lang('sidebar.contribute')</h4>
        <p>@lang('sidebar.contribute.text')</p>
        @include('layouts.contribute')
    </div>
</aside><!-- /.blog-sidebar -->
