@if($errors->any())
    <div class="form-group mt-3">
        <div class="alert alert-danger m-0">
            <ul class="list-group">
                @foreach($errors->all() as $error)
                    <li class="list-nobullet">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
