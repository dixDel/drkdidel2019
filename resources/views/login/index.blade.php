@extends('master')

@section('content')
    <div class="col-md-8">
        <h1>Log in</h1>
        <form action="/drekMansion" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Log in</button>
            </div>

            @include('layouts.errors')
        </form>
     </div>
@endsection
