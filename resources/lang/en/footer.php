<?php

return [
    'license' => '<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" /></a><br />The content of <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">drkdidel.be</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="/en/contact" property="cc:attributionName" rel="cc:attributionURL" title="Contact me">Didier Delhaye</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.',
    'template' => 'Blog template built for <a href="https://getbootstrap.com/">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a> and using <a href="http://www.useiconic.com/open" title="Open Iconic, a free and open icon set.">Open Iconic</a>, a free and open icon set.',
    'back_top' => '<a href="#" title="Back to top">Back to top</a>',
];
