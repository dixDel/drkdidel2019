<?php
return [
    // link to French
    'switch' => 'Aller vers la version française',
    'search' => 'Search',
    'menu-mobile' => [
        'about' => 'About',
        'contact' => 'Contact',
        'archives' => 'Archives',
        'elsewhere' => 'Elsewhere',
        'contribute' => 'Contribute',
    ],
];
