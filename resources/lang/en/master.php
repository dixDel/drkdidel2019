<?php
return [
    'locale' => 'en_US',
    'title' => 'drkdidel.be - notes and thoughts about programming and stuff...',
    'flashContact' => 'Your message has been sent successfully.',
    'nbPosts-single' => 'article',
    'nbPosts' => 'articles',
    'search' => 'Search',
    'paypalSlogan' => 'PayPal - The safer, easier way to pay online!',
    'paypalCountry' => 'BE',
];
