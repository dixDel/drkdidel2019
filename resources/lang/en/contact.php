<?php
return [
    'title' => 'Contact me',
    'subject' => 'Subject',
    'choose' => 'Choose a subject',
    'message' => 'Your message',
    'message_placeholder' => 'Type your message here',
    'email' => 'Email',
    'email_placeholder' => 'Please enter your email address',
    'emailHelp' => 'Your email will not be used for any other purpose without your consent.',
    'name' => 'Name (Optional)',
    'name_placeholder' => 'Enter your name (optional)',
    'send_copy' => 'Check this box to receive a copy of your message.',
    'submit' => 'Send message',
    'back' => 'Back',
    'hello' => 'Greetings',
    'foreword' => 'here is a copy of the message you sent through our contact form',
];
