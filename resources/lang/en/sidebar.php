<?php
return [
    'contact' => [
        'title' => 'Contact me',
        'text1' => 'Please send me your comments, suggestions, job proposals and so on by using',
        'link' => 'Display the contact form',
        'text2' => 'this contact form',
    ],
    'elsewhere'       => 'Elsewhere',
    'contribute'      => 'Contribute',
    'contribute.text' => 'Found something helpful on this site? Please consider helping me keeping it alive.',
    'paypalTitle'     => 'Donate on PayPal',
    'patreonTitle'    => 'Support me on Patreon!',
];
