<?php

return [
    'license' => '<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" /></a><br />Le contenu du site <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">drkdidel.be</span> de <a xmlns:cc="http://creativecommons.org/ns#" href="/fr/contact" property="cc:attributionName" rel="cc:attributionURL" title="Contactez-moi">Didier Delhaye</a> est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/deed.fr">licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.',
    'template' => 'Le modèle de ce blog a été développé pour <a href="https://getbootstrap.com/">Bootstrap</a> par <a href="https://twitter.com/mdo">@mdo</a> et utilise <a href="http://www.useiconic.com/open" title="Open Iconic, a free and open icon set.">Open Iconic</a>, un ensemble d\'icônes libre et ouvert.',
    'back_top' => '<a href="#" title="Retour en haut">Retour en haut</a>',
];
