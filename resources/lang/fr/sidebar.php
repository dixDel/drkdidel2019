<?php
return [
    'contact' => [
        'title' => 'Contactez-moi',
        'text1' => 'Envoyez-moi vos commentaires, suggestions, offres d\'emploi, etc, en utilisant',
        'link' => 'Afficher le formulaire de contact',
        'text2' => 'ce formulaire de contact',
    ],
    'elsewhere' => 'Ailleurs',
    'contribute' => 'Contribuez',
    'contribute.text' => 'Avez-vous trouvé quelque chose d\'utile sur ce site ? Si oui, aidez-moi à le maintenir et le développer. Merci !',
    'paypalTitle'     => 'Faites un don sur PayPal',
    'patreonTitle'    => 'Soutenez-moi sur Patreon!',
];
