<?php
return [
    'title' => 'Contactez-moi',
    'subject' => 'Sujet du message',
    'choose' => 'Choisissez un sujet',
    'message' => 'Votre message',
    'message_placeholder' => 'Tapez votre message ici',
    'email' => 'E-mail',
    'email_placeholder' => 'Veuillez entrer votre adresse e-mail',
    'emailHelp' => 'Votre adresse e-mail ne sera utilisée à aucune autre fin sans votre consentement.',
    'name' => 'Nom (optionnel)',
    'name_placeholder' => 'Entrez votre nom (optionnel)',
    'send_copy' => 'Cochez cette case pour recevoir une copie de votre message',
    'submit' => 'Envoyer',
    'back' => 'Retour',
    'hello' => 'Salutations',
    'foreword' => 'ceci est une copie du message que vous avez envoyé via notre formulaire de contact',
];
