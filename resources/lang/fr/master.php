<?php
return [
    'locale' => 'fr_FR',
    'title' => 'drkdidel.be - notes et réflexions sur la programmation et d\'autres choses...',
    'flashContact' => 'Votre message a bien été envoyé.',
    'nbPosts-single' => 'article',
    'nbPosts' => 'articles',
    'search' => 'Recherche',
    'paypalSlogan' => 'PayPal : la solution simple et sécurisée pour payer et être payé.',
    'paypalCountry' => 'FR',
];
