<?php
return [
    // link to English
    'switch' => 'Switch to English',
    'search' => 'Recherche',
    'menu-mobile' => [
        'about' => 'À propos',
        'contact' => 'Contact',
        'archives' => 'Archives',
        'elsewhere' => 'Ailleurs',
        'contribute' => 'Contribuez',
    ],
];
