<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$adminPrefix = config('app.admin_prefix');
$defaultCategory = strtolower(config('app.categories.en.1'));

Route::get('/', function() use ($defaultCategory) {
    Request::session()->reflash();
    return redirect('en/posts/'.$defaultCategory, 301);
});
Route::redirect('/home', '/en/posts/'.$defaultCategory, 301);

Route::get('{id}/{post?}', 'PostController@showPermalink')->name('permalink');

Route::get('{lang}/contact', 'ContactController@index');
Route::post('{lang}/contact', 'ContactController@send');

Route::prefix('{lang}')->group(function() {
    Route::prefix('posts')->group(function() {
        Route::get('{year}/{month}', 'PostController@archives');
        Route::get('{category}/{post}', 'PostController@show')->name('show');
        Route::post('{category}/{post}', 'CommentController@store');
        Route::get('{category?}', 'PostController@index')->name('home');
    });
    Route::get('tags/{tag}', 'TagController@postsByTag')->name('postsByTag');
    Route::get('search/{keywords?}', 'PostController@search');
});

Route::prefix($adminPrefix)->group(function() {
    Route::get('/', 'LoginController@index')->name('login');
    Route::post('/', 'LoginController@authenticate');
    Route::get('leave', 'LoginController@logout')->middleware('auth');
    Route::get('create', 'PostController@create')->middleware('auth');
    Route::post('posts', 'PostController@store')->middleware('auth');
    Route::get('categories/add/{name}/{name_fr}/{order}', 'CategoryController@store')->middleware('auth');
});

Route::prefix($adminPrefix.'/{lang}/posts')->group(function() {
    Route::get('{id}/edit', 'PostController@editNoSlug')->middleware('auth');
    Route::get('{post}/edit', 'PostController@edit')->middleware('auth');
    Route::get('{post}/translate', 'PostController@translate')->middleware('auth');
    Route::get('{id}/delete', 'PostController@deleteNoSlug')->middleware('auth');
    Route::get('{post}/delete', 'PostController@delete')->middleware('auth');
    Route::patch('{id}', 'PostController@updateNoSlug')->middleware('auth');
    Route::patch('{post}', 'PostController@update')->middleware('auth');
    Route::post('{id}/translate', 'PostController@storeTranslation')->middleware('auth');
});

Route::prefix($adminPrefix.'/media')->group(function() {
    Route::get('{year}/{slug}/{filename}/delete', 'MediaController@delete')->middleware('auth');
});

