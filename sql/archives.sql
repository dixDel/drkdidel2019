SELECT YEAR(created_at) year, MONTH(created_at) month, is_public, COUNT(*) nb_posts
FROM drkdidel.posts
WHERE is_public IS TRUE
GROUP BY year, month
ORDER BY MIN(created_at) DESC